﻿//# *********************************************
//# *				RFID Türöffner				*
//# *			PJ_010_RFID_Türöffner			*
//# *			 	Vorbereitung				*
//# *											*
//# *				 Version 0.1				*
//# *			Created: 2020.02.02				*
//# *		  Last modify: 2020.02.04			* 
//# *			  By M. Hemscheidt				*
//# *********************************************


//Gelesene UID : 56 CD ED 48		// Blauer Tag
//Gelesene UID : 0C E2 B2 70		// Chip Karte

// Bibliotheken einbinden
#include <SPI.h>
#include <MFRC522.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

// Initiale Zuweisung und Adressierung
LiquidCrystal_I2C lcd(0x3F, 20, 4);

// Anschlussbelegung RFID Empfänger
#define RST_PIN   5     // SPI Reset Pin
#define SS_PIN    53    // SPI Slave Select Pin
//      SPI_MOSI  51
//      SPI_MISO  50
//		SPI_SCK   50
MFRC522 mfrc522(SS_PIN, RST_PIN);   // Instanz des MFRC522 erzeugen


// User Identifikations IDs:
byte last_read[128];
byte act_read[128];
byte uid_size;

// Aktive Anzeige
int aktiv = false;
int access_granted = false;
int access_denied = true;
int user = 0;

byte Karte_1[] = { 0x56, 0xCD, 0xED, 0x48 };	//User 1
byte Karte_2[] = { 0x0C, 0xE2, 0xB2, 0x70 };	//User 2


void setup() {

	// Serielle Verbindung starten
	Serial.begin(9600);
	SPI.begin();
	mfrc522.PCD_Init();  // Initialisiere MFRC522 Lesemodul

	// Ladebalken auf dem Display
	boot_display();

}


void loop() {
	

	rfid_einlesen();

	// Chip gescant...
	if (aktiv == true) {

		// Gelbe LED für check einschalten?

		// Feststellen ob karte gelistet ist
		user = check_id();

			// Fall 1: Karte unbekannt	| kein zutritt
				// Gelbe Led Ein
			// Fall 2: Karte gesperrt	| kein zutritt
				// Rote Led Ein
			// Fall 3: Karte OK			|	   zutritt OK
				// Grüne Led Ein

		access_granted = false;
		access_denied = true;
		aktiv = false;
		
	}


	// Kein Chip gescant...
	if (aktiv == false) {

		lcd.setCursor(0, 0);
		lcd.print("Bereit,");
		lcd.setCursor(0, 2);
		lcd.print("Bitte Chip vorhalten");

	}
	


	delay(1000);
  
}

void boot_display() {

	lcd.init();
	lcd.backlight();
	lcd.clear();

	lcd.setCursor(5, 0);
	lcd.print("Booting...");

	for (int i = 0; i <= 19; i++) {
		lcd.setCursor(i, 1);
		lcd.print("*");
		delay(75);
	}

	lcd.setCursor(0, 2);
	lcd.print("RFID Tueroeffnr V0.1");

	lcd.setCursor(4, 3);
	lcd.print("Geschafft...");

	delay(1000);

	lcd.clear();

	return;

}

void rfid_einlesen() {

	if (mfrc522.PICC_IsNewCardPresent() && mfrc522.PICC_ReadCardSerial()) {

		Serial.print("Gelesene UID:");

		for (byte i = 0; i < mfrc522.uid.size; i++) {

			// UID zwischenspeichern
			last_read[i] = mfrc522.uid.uidByte[i];

			Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
			Serial.print(mfrc522.uid.uidByte[i], HEX);

		}

		// UID Länge zwischenspeichern
		uid_size = mfrc522.uid.size;

		// Merker für aktiven auswerteprozess setzen
		aktiv = true;

		mfrc522.PICC_HaltA();

		Serial.println();

		print_uid_display();

	}
}

void print_uid_display() {

	lcd.backlight();
	lcd.clear();

	lcd.setCursor(0, 0);
	lcd.print("Eingelesen...");

	lcd.setCursor(0, 2);
	lcd.print("ID: ");

	for (byte i = 0; i < uid_size; i++) {
		lcd.print(last_read[i] < 0x10 ? "0" : "");
		lcd.print(last_read[i], HEX);
		lcd.print("");
	}

	delay(2000);

	lcd.clear();
	// lcd.noBacklight();

	return;

}

int check_id() {

	int check = false;
	int id = 0;


	for (int i = 0; i <= uid_size; i++) {
		if (last_read[i], HEX != Karte_1[i], HEX) {
			Serial.println("Check auf Blauen Chip");
			Serial.print(Karte_1[i], HEX);
			Serial.print(last_read[i], HEX);
			Serial.println();
			check = false;

			//i = 0;

			//break;
		}
		if (i == uid_size) {
			check = true;
			id = 1;
			Serial.println("Blauer Chip");

			lcd.setCursor(0, 2);
			lcd.print("Alina Bartenstein");

			delay(2000);

			lcd.clear();

		}
	}

	for (int i = 0; i <= uid_size; i++) {
		if (last_read[i], HEX != Karte_2[i], HEX) {
			Serial.println("Check auf Weiße Karte");
			Serial.print(Karte_2[i], HEX);
			Serial.print(last_read[i], HEX);
			Serial.println();
			check = false;

			//i = 0;

			//break;
		}
		if (i == uid_size) {
			check = true;
			id = 2;
			Serial.println("Weiße Karte");
		}

		return id;

	}



	/*

	// Blauer Tag
	if ((Karte_1, HEX) == (last_read, HEX)) {

		access_denied = false;
		access_granted = true;
		Serial.println("Blauer Chip");

	}

	// Weisse Karte
	if (Karte_2 == last_read) {

		access_denied = false;
		access_granted = true;
		Serial.println("Weisse Karte");

	}

	*/
}

