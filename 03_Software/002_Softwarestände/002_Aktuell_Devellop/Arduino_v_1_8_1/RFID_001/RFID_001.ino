//# *********************************************
//# *             RFID Türöffner                *
//# *         PJ_010_RFID_Türöffner             *
//# *               Vorbereitung                *
//# *                                           *
//# *               Version 0.1                 *
//# *           Created: 2020.02.02             *
//# *         Last modify: 2020.02.02           * 
//# *           By A. Bartenstein               *
//# *********************************************

#include <Wire.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 20, 4);

int RoteLed = 50;
int GelbeLed = 51;
int GrueneLed = 52;


void setup() 
{

  pinMode(RoteLed, OUTPUT);    //Ausgang für Rote Led
  pinMode(GelbeLed, OUTPUT);    //Ausgang für Gelbe Led
  pinMode(GrueneLed, OUTPUT);    //Ausgang für Grüne Led
  
  lcd.init();         // Lcd Initialisieren

  digitalWrite(RoteLed, HIGH);
  delay(250);
  digitalWrite(GelbeLed, HIGH);
  delay(250);
  digitalWrite(GrueneLed, HIGH);
  delay(250);

  digitalWrite(RoteLed, LOW);
  digitalWrite(GelbeLed, LOW);
  digitalWrite(GrueneLed, LOW);

}



void loop() 
{

  digitalWrite(RoteLed, HIGH);
  digitalWrite(GelbeLed, HIGH);
  digitalWrite(GrueneLed, HIGH);

  lcd.backlight();
  lcd.setCursor(0,0);
  lcd.print("Hallo ");
  
  delay(1000);
  
  lcd.clear();
  
  digitalWrite(RoteLed, LOW);
  digitalWrite(GelbeLed, LOW);
  digitalWrite(GrueneLed, LOW);

  delay(1000);

}
